// Bài 5: In số nguyên tố tới giá trị nhập n
function ketQuaB5(){
  var numberB5 = document.getElementById('numberB5').value*1;
  var outputStringB5 = '';

  for(var n = 2; n <= numberB5; n++){
    var checkSNT = checkPrimeNumber(n);
    if(checkSNT){
      outputStringB5 += `${n} `;
    }
  }
  document.getElementById('showKetQuaB5').innerHTML = outputStringB5;
}
