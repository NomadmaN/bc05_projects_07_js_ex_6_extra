
// This function to check if a number is Prime number or not
function checkPrimeNumber(number){
    var checkPrimeNumber = true;
    for (var i = 2; i <= Math.sqrt(number); i++){
      if (number % i == 0){
        checkPrimeNumber = false;
        break;
      }
    }
    return checkPrimeNumber;
  }